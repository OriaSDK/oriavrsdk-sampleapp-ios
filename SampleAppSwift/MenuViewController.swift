//
//  ViewController.swift
//  SampleAppSwift
//
//  Created by Pablo  Gómez on 5/2/16.
//  Copyright © 2016 OriaVR. All rights reserved.
//

import UIKit

class MenuViewController: UIViewController, OriaVideoDetailViewControllerDelegate {
    // Instance variables
    @IBOutlet var videoListButton:UIButton!
    @IBOutlet var videoButton:UIButton!
    @IBOutlet var activityIndicator:UIActivityIndicatorView!
    
    var video:OriaVideo?

    override func loadView() {
        super.loadView()
        
        self.videoListButton.titleLabel?.font = UIFont(name: "Ubuntu", size: 16.0)
        self.videoListButton.layer.cornerRadius = 5.0
        self.videoListButton.layer.borderWidth = 1.0
        self.videoListButton.layer.borderColor = self.videoListButton?.titleLabel?.textColor.CGColor
        
        self.videoButton.titleLabel?.font = UIFont(name: "Ubuntu", size: 16.0)
        self.videoButton.layer.cornerRadius = 5.0
        self.videoButton.layer.borderWidth = 1.0
        self.videoButton.layer.borderColor = self.videoButton?.titleLabel?.textColor.CGColor
    }
    
    @IBAction func showVideo(sender: AnyObject) {
        if video == nil {
            videoButton.hidden = true
            activityIndicator.startAnimating()
            
            OriaSDK.sharedSDK()?.videoWithIdentifier("08Ogp9Dz", callback: { (video, error) in
                dispatch_async(dispatch_get_main_queue(), { 
                    self.videoButton?.hidden = false
                    self.activityIndicator?.stopAnimating()
                    
                    if let video = video {
                        self.video = video
                        self.presentVideoDetailsViewController()
                    } else {
                        //If there is an error we just print it. In your application you should handle this error appropiately.
                        print(error)
                    }
                })
            })
        } else {
            presentVideoDetailsViewController()
        }
    }
    
    func presentVideoDetailsViewController() {
        let viewController = OriaVideoDetailViewController(video: video!)
        viewController.delegate = self
        presentViewController(viewController, animated: true, completion: nil)
    }
    
    // OriaVideoDetailViewControllerDelegate
    
    func oriaVideoDetailViewControllerShouldDismiss(viewController: OriaVideoDetailViewController) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
}

