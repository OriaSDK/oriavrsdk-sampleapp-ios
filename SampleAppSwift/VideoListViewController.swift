//
//  ViewController.swift
//  SampleAppSwift
//
//  Created by Pablo  Gómez on 5/2/16.
//  Copyright © 2016 OriaVR. All rights reserved.
//

import UIKit

class VideoListViewController: UITableViewController {
    var videos:[OriaVideo]?
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        if videos == nil {
            OriaSDK.sharedSDK()?.videos({ (videos, error) in
                dispatch_async(dispatch_get_main_queue(), { 
                    if let videos = videos  {
                        self.videos = videos;
                        self.tableView.reloadData()
                    } else {
                        print("Error \(error)")
                    }
                })
            })
        }
    }
    
    // Table View Data Source
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let videos = videos {
            return videos.count
        } else {
            return 0
        }
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("videoCell") as? VideoTableViewCell
        cell?.configureWithVideo(videos![indexPath.row])
        print(videos![indexPath.row].identifier)
        
        return cell!
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let videoDetailViewController = OriaVideoDetailViewController(video: videos![indexPath.row])
        navigationController?.pushViewController(videoDetailViewController, animated: true)
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return tableView.bounds.size.width * 335.0 / 476.0
    }
}

class VideoTableViewCell: UITableViewCell {
    @IBOutlet var screenshotImageView:UIImageView!
    @IBOutlet var titleLabel:UILabel!
    @IBOutlet var sizeLabel:UILabel!
    @IBOutlet var activityIndicator:UIActivityIndicatorView!
    @IBOutlet var gradientView:UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.titleLabel.font = UIFont(name: "Ubuntu-Bold", size: 21.0)
        self.sizeLabel.font = UIFont(name: "Ubuntu", size: 14.0)
        
        let gradient = CAGradientLayer()
        gradient.frame = self.gradientView.bounds
        gradient.colors = [UIColor.clearColor(), UIColor.blackColor()]
        gradientView.layer.insertSublayer(gradient, atIndex: 0)
    }
    
    func configureWithVideo(video:OriaVideo) {
        titleLabel.text = video.title;
        sizeLabel.text = "\(video.durationString) | \(video.sizeString)"
        
        let image = video.image { (image, error) in
            if image != nil {
                self.screenshotImageView.image = image;
                self.activityIndicator.stopAnimating()
            }
        }
        
        if image != nil {
            self.screenshotImageView.image = image;
            self.activityIndicator.stopAnimating()
        } else {
            self.screenshotImageView.image = nil;
            self.activityIndicator.startAnimating()
        }
    }
}

