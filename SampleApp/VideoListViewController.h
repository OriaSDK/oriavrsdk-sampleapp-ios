#import <UIKit/UIKit.h>
#import "OriaSDK.h"
#import "OriaVideo.h"

@interface VideoListViewController : UITableViewController


@end

@interface VideoTableViewCell : UITableViewCell

@property (nonatomic, retain) IBOutlet UIImageView *screenshotImageView;
@property (nonatomic, retain) IBOutlet UILabel *titleLabel;
@property (nonatomic, retain) IBOutlet UILabel *sizeLabel;
@property (nonatomic, retain) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (nonatomic, retain) IBOutlet UIView *gradientView;

- (void)configureWithVideo:(OriaVideo *)video;

@end

