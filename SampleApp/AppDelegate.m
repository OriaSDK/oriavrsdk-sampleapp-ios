#import "AppDelegate.h"
#import "OriaVideoDetailViewController.h"
#import "OriaSDK.h"

// Conform to the OriaSDKShareDelegate protocol.
@interface AppDelegate ()<OriaSDKShareDelegate>

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    // Set the OriaSDK token. This is a sample token, for use in your own app you MUST replace this token.
    [OriaSDK provideAPIToken:@"test_HE1Mz8F0hMij"];
    
    // For simplicity, we set the app delegate as the share delegate for the oriaSDK. In your own application you will probably want to use another object for this purpose.
    [OriaSDK sharedSDK].shareDelegate = self;
    return YES;
}

-(NSArray *)activityItemsForSharingVideo:(OriaVideo *)video {
    return [NSArray arrayWithObjects:[NSString stringWithFormat:@"Check out %@ at OriaVR!", video.title], [NSURL URLWithString:@"https://www.oriavr.com"], nil];
}

// Make the navigation controller defer the check of supported orientation to its topmost view
// controller. This allows |GCSCardboardViewController| to lock the orientation in VR mode.
- (UIInterfaceOrientationMask)navigationControllerSupportedInterfaceOrientations:
(UINavigationController *)navigationController {
    return [navigationController.topViewController supportedInterfaceOrientations];
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
