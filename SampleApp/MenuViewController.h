#import <UIKit/UIKit.h>

@interface MenuViewController : UIViewController

- (IBAction)showVideo:(id)sender;

@property (nonatomic, retain) IBOutlet UIButton *videoListButton;
@property (nonatomic, retain) IBOutlet UIButton *videoButton;
@property (nonatomic, retain) IBOutlet UIActivityIndicatorView *activityIndicator;

@end

