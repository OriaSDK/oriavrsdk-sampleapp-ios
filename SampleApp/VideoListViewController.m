#import "VideoListViewController.h"
#import "OriaVideoDetailViewController.h"

@interface VideoListViewController ()

@property (retain, nullable) NSArray <OriaVideo *> *videos;

@end

@implementation VideoListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if(self.videos == nil) {
        [[OriaSDK sharedSDK] videos:^(NSArray<OriaVideo *> * _Nullable videos, NSError * _Nullable error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                if(videos != nil) {
                    self.videos = videos;
                    [self.tableView reloadData];
                } else {
                    NSLog(@"Error: %@", error);
                }
            });
        }];
    }
    
}

#pragma Table View Data Source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.videos.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    VideoTableViewCell *cell = (VideoTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"videoCell"];
    [cell configureWithVideo:self.videos[indexPath.row]];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    OriaVideoDetailViewController *videoDetailViewController = [[OriaVideoDetailViewController alloc] initWithVideo:self.videos[indexPath.row]];
    [self.navigationController pushViewController:videoDetailViewController animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    // Keep a 476:335 aspect ratio for cells.
    return tableView.bounds.size.width * 335.0f / 476.0f;
}

@end

@implementation VideoTableViewCell

-(void)awakeFromNib {
    [super awakeFromNib];
    
    self.titleLabel.font = [UIFont fontWithName:@"Ubuntu-Bold" size:21.0f];
    self.sizeLabel.font = [UIFont fontWithName:@"Ubuntu" size:14.0f];
    
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = self.gradientView.bounds;
    gradient.colors = [NSArray arrayWithObjects:(id)[[UIColor clearColor] CGColor], (id)[[UIColor blackColor] CGColor], nil];
    [self.gradientView.layer insertSublayer:gradient atIndex:0];
}

- (void)configureWithVideo:(OriaVideo *)video {
    self.titleLabel.text = video.title;
    self.sizeLabel.text = [NSString stringWithFormat:@"%@ | %@", video.durationString, video.sizeString];
    
    UIImage *image = [video image:^(UIImage * _Nullable image, NSError * _Nullable error) {
        if(image != nil) {
            self.screenshotImageView.image = image;
            [self.activityIndicator stopAnimating];
        }
    }];
    if(image) {
        self.screenshotImageView.image = image;
        [self.activityIndicator stopAnimating];
    } else {
        self.screenshotImageView.image = nil;
        [self.activityIndicator startAnimating];
    }
}

@end
