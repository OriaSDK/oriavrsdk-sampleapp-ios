#import "MenuViewController.h"
#import "OriaSDK.h"
#import "OriaVideoDetailViewController.h"

@interface MenuView Controller () <OriaVideoDetailViewControllerDelegate>

- (IBAction)showVideo:(id)sender;
- (void)presentVideoDetailsViewController;

@property (nonatomic, retain) OriaVideo *video;

@end

@implementation MenuViewController

-(void)loadView {
    [super loadView];

    self.videoListButton.titleLabel.font = [UIFont fontWithName:@"Ubuntu" size:16.0f];
    self.videoListButton.layer.cornerRadius = 5.0f;
    self.videoListButton.layer.borderWidth = 1.0f;
    self.videoListButton.layer.borderColor = self.videoListButton.titleLabel.textColor.CGColor;
    
    self.videoButton.titleLabel.font = [UIFont fontWithName:@"Ubuntu" size:16.0f];
    self.videoButton.layer.cornerRadius = 5.0f;
    self.videoButton.layer.borderWidth = 1.0f;
    self.videoButton.layer.borderColor = self.videoButton.titleLabel.textColor.CGColor;
}

- (IBAction)showVideo:(id)sender {
    if(self.video == nil) {
        self.videoButton.hidden = YES;
        [self.activityIndicator startAnimating];
        
        [[OriaSDK sharedSDK] videoWithIdentifier:@"08Ogp9Dz" callback:^(OriaVideo * _Nullable video, NSError * _Nullable error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                self.videoButton.hidden = NO;
                [self.activityIndicator stopAnimating];
                
                if(video) {
                    self.video = video;
                    [self presentVideoDetailsViewController];
                } else {
                    // If there is an error we just print it. In your application you should handle this error appropiately.
                    NSLog(@"%@", error);
                }
            });
        }];
    } else {
        [self presentVideoDetailsViewController];
    }
}

- (void)presentVideoDetailsViewController {
    OriaVideoDetailViewController *viewController = [[OriaVideoDetailViewController alloc] initWithVideo:self.video];
    viewController.delegate = self;
    [self presentViewController:viewController animated:YES completion:nil];
}

#pragma mark OriaVideoDetailViewControllerDelegate

-(void)oriaVideoDetailViewControllerShouldDismiss:(OriaVideoDetailViewController *)viewController {
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
