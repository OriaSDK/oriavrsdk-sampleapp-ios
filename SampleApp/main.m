//
//  main.m
//  SampleApp
//
//  Created by Pablo  Gómez on 5/2/16.
//  Copyright © 2016 OriaVR. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
