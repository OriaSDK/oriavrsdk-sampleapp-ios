
# OriaSDK

OriaSDK allows you to easily show 360 videos hosted in OriaVR inside your app.

## Features

* Quick setup. You tell us which video to show, we take care of the rest.
* Works with iOS 8.0+


## Installation

OriaSDK requires [CocoaPods](http://cocoapods.org/) for installation. You can install CocoaPods with the following command:

```bash
$ gem install cocoapods
```

To integrate OriaSDK into you Xcode project, specify it in your `Podfile`:

```ruby
source 'https://github.com/CocoaPods/Specs.git'

pod 'OriaSDK, '~> 0.1'
```
> During our beta phase, OriaSDK is not available in the main CocoaPods repository so you must include our private pod repository as a source in your `Podfile`.

Then, run the following command:

```bash
$ pod install
```

## Usage

### Initializing OriaSDK

Before using any of the views or methods in OriaSDK, you must first initialize the SDK with your application's token.

> Calling any method provided by the SDK, without setting the token first will return `nil` and print a message in the application's log.

If you don't have an application token yet, head over to our [developer portal](https://developer.oriavr.com) to get one.

To initialize the OriaSDK with your token just call:

> Swift
>
```swift
OriaSDK.provideAPIToken("YOUR-TOKEN-GOES-HERE")
```

<!-- -->
> Objective-C
>
```objective-c
[OriaSDK provideAPIToken:@"YOUR-TOKEN-GOES-HERE"];
```

### Getting Videos

OriaSDK uses the `OriaVideo` object to represent a video available for viewing. There are two main ways to get `OriaVideo` objects. To get a list of all videos available with the specified token, use the `videos(callback:)` method in `OriaSDK`:

> Swift
>
```swift
OriaSDK.sharedSDK()?.videos({ (videos, error) in
    if let videos = videos  {
        // Do something with the list of videos.
    } else {
        // Handle any errors when retrieveing the video list.     
    }
})
```

<!-- -->
> Objective-C
>
```objective-c
[[OriaSDK sharedSDK] videos:^(NSArray<OriaVideo *> * _Nullable videos, NSError * _Nullable error) {
    if(videos != nil) {
        // Do something with the list of videos.
    } else {
        // Handle any errors when retrieveing the video list. 
    }
}];
```

<!-- -->
> **IMPORTANT:** The callback function passed to `videos(callback:)` is not guaranteed to be called in the main thread. If you want to update UI elements inside this callback, make sure you execute the code block in the main thread using `dispatch_async`.

Alternatively, if you know the identifier of a specific video, you can use the `videoWithIdentifier(_:callback:)` method in `OriaSDK`

> Swift
>
```swift
OriaSDK.sharedSDK()?.videoWithIdentifier("VIDEO-IDENTIFIER", callback: { (video, error) in
    if let video = video {
        // Do something with the video.
    } else {
        // Handle any errors when retrieveing the video.
    }
})
```

<!-- -->
> Objective-C
>
```objective-c
[[OriaSDK sharedSDK] videoWithIdentifier:@"VIDEO-IDENTIFIER" callback:^(OriaVideo * _Nullable video, NSError * _Nullable error) {
    if(video != nil) {
        // Do something with the video.
    } else {
        // Handle any errors when retrieveing the video.
    }
}];
```

<!-- -->
> **IMPORTANT:** The callback function passed to `videoWithIdentifier(_:callback:)` is not guaranteed to be called in the main thread. If you want to update UI elements inside this callback, make sure you execute the code block in the main thread using `dispatch_async`.
 
### Working with `OriaVideo` Objects

Once you have an `OriaVideo` object, you may retrieve the following information about a video using properties:

* `identifier` - Contains the identifier for the video.
* `title` - Contains the title for the video.
* `summary ` - Contains the description for the video.
* `image` - An instance of `UIImage` with the image for the video, if available. See below for more information.
* `public ` - Contains whether the video is available publicly in OriaVR.com or not.
* `size ` - Contains the file size of the video in MB.
* `duration ` - Contains the duration of the video in seconds.
* `sizeString ` - A convenience property that returns a formatted string for the video size. See the documentation for `OriaVideo` for more information.
* `durationString ` - A convenience property that returns a formatted string for the duration of the video. See the documentation for `OriaVideo` for more information.
* `isImageDownloaded` - Contains whether the image for this video has been downloaded or not.

Additionally, you may use `OriaVideo` to download an image that represents the video.  The `image(callback:)` method will return an `UImage` object if the image has already been downloaded, or `nil` if the image needs to be downloaded. If the image needs to be downloaded, then `callback` will be executed when the download is complete. The suggested way to do this is as follows:

> Swift
>
```swift
let image = video.image { (image, error) in
    // The image needs to be downloaded. When the download is complete, this callback will be invoked.
    if let image = image {
        self.myImageView.image = image;
    }
}
// The image was already available, it can be used straight away. The callback will never be executed.
if let image = image {
    myImageView.image = image;
}
```

<!-- -->
> Objective-C
>
```objective-c
UIImage *image = [video image:^(UIImage * _Nullable image, NSError * _Nullable error) {
    // The image needs to be downloaded. When the download is complete, this callback will be invoked.
    if(image != nil) {
        self.myImageView.image = image;
    }
}];
// The image was already available, it can be used straight away. The callback will never be executed.
if(image != nil) {
    self.myImageView.image = image;
}
```

<!-- -->
> Unlike `videos(callback:)` and `videoWithIdentifier(_:callback:)`, the callback function passed to `image(callback:)` will always be executed in the main thread.


### Using `OriaVideoDetailViewController`

To present a video for viewing, you should use `OriaVideoDetailViewController`:

> Swift
>
```swift
let video:OriaVideo = [...]
let videoDetailViewController = OriaVideoDetailViewController(video: video)
navigationController?.pushViewController(videoDetailViewController, animated: true)
```

<!-- -->
> Objective-C
>
```objective-c
OriaVideo *video = [...]
OriaVideoDetailViewController * videoDetailViewController = [[OriaVideoDetailViewController alloc] initWithVideo:self.video];
[self.navigationController pushViewController:videoDetailViewController animated:YES];
```

When presenting `OriaVideoDetailViewController` outside of a `UINavigationController`, the `OriaVideoDetailViewController` will show a "Back" button to the user. The caller should implement the `OriaVideoDetailViewControllerDelegate` protocol and set itself as the `videoDetailViewController ` delegate before presentation. Read the documentation for `OriaVideoDetailViewController` for more information.

### Sharing Videos

Some of the views presenting by the OriaSDK have the option to display a "Share" button. The "Share" button will present the standard iOS Share Sheet. To control the content that will be shared when the user performs this action, you should implement the `OriaSDKShareDelegate` protocol. You can read more about this protocol in our documentation, or check out a sample implementation in the SampleApp's `AppDelegate`.

## Support 

Please report any bugs or feature requests in our [sample app repository](http://www.github.com).

## License

OriaSDK is released under the Healtherus, Inc. Software Development Kit License. See LICENSE.md for details.
    